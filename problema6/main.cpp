/*Requerimientos: Se requiere que el ususario ingrese una cadena de caracteres.

  Garantiza: Que reciba una cadena de caracteres y cambie las letras minúsculas por mayúsculas,
  los demás caracteres no deben ser alterados. */

#include <iostream>
#include <string.h>
#include <stdlib.h>

using namespace std;

int main()
{
    char cadena[1000]={};//se declara la cadena principal dandole el tamaño por defecto
    char cadena_aux[1000]={};// se declara una cadena auxiliar donde estaran la cadena ya convertida en mayuscula

    char *p=cadena, *p_aux=cadena_aux;//se declaran dos punteros que nos ayudaran a movernos por todas las posiciones de las cadenas

    cout << "Ingrese una cadena" << endl;
    cin >> cadena;
    int l1=strlen(cadena);//instruccion que da la longitud de la cadena ingresada

    for (int i=0; i<l1; i++){//ciclo que recorre cada posicion de la cadena
        if(*(p+i)>=97 && *(p+i)<=122){//instruccion que me compara si las letras son minusculas
            //cout << *(p+i)-32 << endl;
            *(p_aux+i) = *(p+i)-32;//al puntero auxiliar se le asigna la letra minusculas convertida en mayuscula del puntero original

        }
        else {
         *(p_aux+i) = *(p+i);// de lo contrario en caso de que la letra no sea mayuscula se le asigna al puntero auxiliar sin convertirla
        }

    }
    cout << "Original = ";
    for(int i=0; i<l1; i++)//ciclo para imprimir la cadena original
        {

            cout<<*(p+i);
        }
    cout << ", En mayusculas = ";
    for(int i=0; i<l1; i++)//ciclo para imprimir la cadena auxiliar
        {

            cout<<*(p_aux+i);
        }

         cout<<endl ;
    return 0;
}
